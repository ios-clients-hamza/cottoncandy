import UIKit

class signUpCollectionViewCell: UICollectionViewCell {
    
    //CELL 1
    @IBOutlet weak var cell1BusinessNameTextField: UITextField!
    @IBOutlet weak var cell1CategoryTextField: UITextField!
    @IBOutlet weak var cell1NextButtonView: ShadowView!
    @IBOutlet weak var cell1NextButton: UIButton!
    @IBOutlet weak var cell1CategoryView: UIView!
    
    //CELL 2
    @IBOutlet weak var cell2PlacePickerMapButton: UIButton!
    @IBOutlet weak var cell2BusinessAddressTextField: UITextField!
    @IBOutlet weak var cell2BusinessPhoneNoTextField: UITextField!
    @IBOutlet weak var cell2NextButtonView: ShadowView!
    @IBOutlet weak var cell2NextButton: UIButton!
    
    //CELL 3
    @IBOutlet weak var cell3EmailTextField: UITextField!
    @IBOutlet weak var cell3PasswordTextField: UITextField!
    @IBOutlet weak var cell3RePasswordTextField: UITextField!
    @IBOutlet weak var cell3NextButtonView: ShadowView!
    @IBOutlet weak var cell3NextButton: UIButton!
    
}
