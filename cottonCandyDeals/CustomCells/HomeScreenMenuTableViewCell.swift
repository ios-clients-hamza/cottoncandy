import UIKit

class HomeScreenMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var catBar: UIView!
    @IBOutlet weak var catView: UIView!
    @IBOutlet weak var catLabel: UILabel!

}
