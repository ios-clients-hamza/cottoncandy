import UIKit

class HomeScreenStatusTableViewCell: UITableViewCell {

    @IBOutlet weak var removeCategoryButton: UIButton!
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!

}
