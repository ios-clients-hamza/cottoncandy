import UIKit

class profileStatusInfo: NSObject {
    
    var addressUser: String?
    var business_nameUser: String?
    var category_idUser: String?
    var category_titleUser: String?
    var emailUser: String?
    var phoneUser: String?
    var user_idUser: String?
    
    var addressStatus: String?
    var business_nameStatus: String?
    var category_titleStatus: String?
    var deal_forStatus: String?
    var phoneStatus: String?
    var status_imageStatus: String?
    var total_viewedStatus: Int?
    var user_idStatus: String?
    var user_status_idStatus: String?
    
    var user_status = Array<Any>()
    
    class func parseProfileStatusInfo(dict: NSDictionary) -> profileStatusInfo {
        
        let userObject = profileStatusInfo()
        
        let userInfo = dict["user_info"] as! NSDictionary
        
        userObject.addressUser = userInfo["address"] as? String
        userObject.business_nameUser = userInfo["business_name"] as? String
        userObject.category_idUser = userInfo["category_id"] as? String
        userObject.category_titleUser = userInfo["category_title"] as? String
        userObject.emailUser = userInfo["email"] as? String
        userObject.phoneUser = userInfo["phone"] as? String
        userObject.user_idUser = userInfo["user_id"] as? String
        
        let userStatus = dict["user_status"] as! NSArray
        
        for item in userStatus {
            
            let statusObject = profileStatusInfo()
            let dict = item as! NSDictionary
            
            statusObject.addressStatus = dict["address"] as? String
            statusObject.business_nameStatus = dict["business_name"] as? String
            statusObject.category_titleStatus = dict["category_title"] as? String
            statusObject.deal_forStatus = dict["deal_for"] as? String
            statusObject.phoneStatus = dict["phone"] as? String
            statusObject.status_imageStatus = dict["status_image"] as? String
            statusObject.total_viewedStatus = dict["total_viewed"] as? Int
            statusObject.user_idStatus = dict["user_id"] as? String
            statusObject.user_status_idStatus = dict["user_status_id"] as? String
            
            userObject.user_status.append(statusObject)
            
        }
        
        return userObject
    }
    
}
