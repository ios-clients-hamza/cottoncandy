import UIKit

class catAndStatusData: NSObject {
    
    var category_idCat: String?
    var colorCat: String?
    var titleCat: String?
    
    var addressUser: String?
    var business_nameUser: String?
    var category_idUser: String?
    var emailUser: String?
    var phoneUser: String?
    var user_idUser: String?
    var over_all_viewedUser: Int?
    
    var addressStatus: String?
    var business_nameStatus: String?
    var deal_forStatus: String?
    var is_viewedStatus: Int?
    var phoneStatus: String?
    var status_imageStatus: String?
    var total_viewedStatus: Int?
    var user_idStatus: String?
    var user_status_idStatus: String?
    
    var user_user = Array<Any>()
    var user_status = Array<Any>()
    
    class func parseCategoriesInfo(dict: NSDictionary) -> Array<Any> {
        print(dict)
        
        var tempArray = Array<Any>()
        
        let catInfo = dict["categories"] as! NSArray
        
        for item in catInfo {
            print(item as! NSDictionary)
            
            let catObject = catAndStatusData()
            
            let dict = item as! NSDictionary
            
            catObject.category_idCat = dict["category_id"] as? String
            catObject.colorCat = dict["color"] as? String
            catObject.titleCat = dict["title"] as? String
            
            let userInfo = dict["users"] as! NSArray
            print(userInfo)
            
            for item in userInfo {
                print(item as! NSDictionary)
                
                let userObject = catAndStatusData()
                let dict = item as! NSDictionary
                
                userObject.addressUser = dict["address"] as? String
                userObject.business_nameUser = dict["business_name"] as? String
                userObject.category_idUser = dict["category_id"] as? String
                userObject.emailUser = dict["email"] as? String
                userObject.phoneUser = dict["phone"] as? String
                userObject.user_idUser = dict["user_id"] as? String
                userObject.over_all_viewedUser = dict["over_all_viewed"] as? Int
                
                let statusInfo = dict["user_status"] as! NSArray
                print(statusInfo.count)
                
                if statusInfo.count != 0 {
                    
                    for item in statusInfo {
                        print(item as! NSDictionary)
                        
                        let statusObject = catAndStatusData()
                        let dict = item as! NSDictionary
                        
                        statusObject.addressStatus = dict["address"] as? String
                        statusObject.deal_forStatus = dict["deal_for"] as? String
                        statusObject.phoneStatus = dict["phone"] as? String
                        statusObject.status_imageStatus = dict["status_image"] as? String
                        statusObject.user_idStatus = dict["user_id"] as? String
                        statusObject.user_status_idStatus = dict["user_status_id"] as? String
                        statusObject.business_nameStatus = dict["business_name"] as? String
                        statusObject.total_viewedStatus = dict["total_viewed"] as? Int
                        statusObject.is_viewedStatus = dict["is_viewed"] as? Int
                        
                        userObject.user_status.append(statusObject)
                    }
                    
                    catObject.user_user.append(userObject)
                    
                } else {
                    
                }
                
            }
            
            tempArray.append(catObject)
        }
        
        return tempArray
    }
    
}
