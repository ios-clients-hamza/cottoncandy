import UIKit

class UserInformation: NSObject {
    
    var user_id: String?
    var email: String?
    var business_name: String?
    var address: String?
    var phone: String?
    var category_id: String?
    var category_title: String?
    
}
