import UIKit

class addStatusViewController: BaseClass, UITextViewDelegate {
    
    @IBOutlet weak var statusAddressLabel: UILabel!
    @IBOutlet weak var statusPhoneNumberLabel: UILabel!
    @IBOutlet weak var outerImageView: UIImageView!
    @IBOutlet weak var innerImageView: UIImageView!
    @IBOutlet weak var postStatusButton: UIButton!
    @IBOutlet weak var dealTextView: UITextView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var selectedImage: UIImage? = nil
    
    let userInfo = UserDefaults.standard.value(forKey: "userInfoDict") as! NSDictionary
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dealTextView.delegate = self
        
        outerImageView.clipsToBounds = true
        
        hideKeyboard()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        statusAddressLabel.text = userInfo["address"] as? String
        statusPhoneNumberLabel.text = userInfo["phone"] as? String
        
        outerImageView.image = selectedImage
        innerImageView.image = selectedImage
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func postButton(_ sender: UIButton) {
        
        if !checkInternet() {
            return
        }
        
        startActivityIndicator(activityIndicator: activityIndicator)
        
        var parameters = [String: Any]()
        
        parameters["user_id"] = userInfo["user_id"] as! String
        parameters["address"] = statusAddressLabel.text!
        parameters["deal_for"] = dealTextView.text!
        parameters["status_image"] = innerImageView.image!
        parameters["status_latitude"] = userInfo["user_latitude"] as! String
        parameters["status_longitude"] = userInfo["user_longitude"] as! String
        
        print(parameters)
        
        let url = BASE_URL + SAVE_STATUS
        
        ApiManager.sharedInstance.alamofirePostRequestWithImage(urlString: url, parameters: parameters, successCallback: { (dict) in
            
            print(dict)
            
            if UserDefaults.standard.value(forKey: "userProfileStatus") != nil {
                UserDefaults.standard.removeObject(forKey: "userProfileStatus")
            }
            
            let imageData: Data = NSKeyedArchiver.archivedData(withRootObject: self.innerImageView.image!)
            UserDefaults.standard.set(imageData, forKey: "userProfileStatus")
            
            self.stopActivityIndicator(activityIndicator: self.activityIndicator)
            
            let push = self.storyboard?.instantiateViewController(withIdentifier: "gotoProfileViewController") as! profileViewController
            self.navigationController?.pushViewController(push, animated: true)
            
            
        }) { (error) in
            self.stopActivityIndicator(activityIndicator: self.activityIndicator)
            self.showAlertForForm(title: "Error", message: error, isPopToRoot: false)
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.text = ""
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        //resizing textView
        let fixedWidth = textView.frame.size.width
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        textView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        
        //limiting textView
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars <= 140
        
    }

}
