import UIKit
import GooglePlaces
import SwiftReorder

class HomeScreenViewControllerNew: BaseClass, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, CLLocationManagerDelegate {

    //    , dataPassingFromShowStatusVC
    
//    func dataPassingFromShowStatus(isStatusViewed: Bool) {
//        self.isStatusViewed = isStatusViewed
//    }
    
    
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var statusTableView: UITableView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var bottomCircle: UIView!
    @IBOutlet weak var bottomMidCircle: UIView!
    @IBOutlet weak var sideMenuEqualHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var homeControllerTableViewRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var sideMenuButton: UIButton!
    @IBOutlet weak var rightCircleConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftCircleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftCircleBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let locationManager = CLLocationManager()
    var placesClient: GMSPlacesClient!
    var lat: String? = nil
    var long: String? = nil
    
    var url: String = ""
    var parameters = [String: Any]()
    
    var catCounter: Int = 0
//    var isStatusViewed: Bool = false
    
    //    var refreshControl = UIRefreshControl()
    //var isRefreshCalled = false
    
    var globalDataArray = Array<Any>()
    var dummyStatus = Array<Any>()
    
    var chypiCheck: Int = 0
    var IsViewWillAppear: Bool = false
    
    //    var isSideMenuButtonAlreadySelected: Bool = false
    var isTimerTime: Bool = false
    var isDropDown: Bool = false
    
//    var delegateInHomeScreen: dataPassingFromShowStatusVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
        placesClient = GMSPlacesClient.shared()
        
        roundTopView(view: topView)
        roundTopView(view: whiteView)
        bottomCircle.layer.cornerRadius = bottomCircle.frame.height / 2
        bottomCircle.clipsToBounds = true
        bottomCircle.layer.borderWidth = 13
        bottomCircle.layer.borderColor = #colorLiteral(red: 0.2588235294, green: 0.8666666667, blue: 0.7725490196, alpha: 1)
        bottomMidCircle.layer.cornerRadius = bottomMidCircle.frame.height / 2
        bottomMidCircle.clipsToBounds = true
        
        //        refreshControl.tintColor = #colorLiteral(red: 0.337254902, green: 0.5843137255, blue: 0.9254901961, alpha: 1)
        //        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        //menuTableView.addSubview(refreshControl)
        
        generateRandomStringForGuestUserOneTime()
        
        statusTableView.reorder.delegate = self
        statusTableView.reorder.shadowOpacity = 0.5
        statusTableView.reorder.shadowRadius = 20
        
        timerRefresher()
    }
    
    //    @objc func refresh() {
    //
    //        chypiCheck = 0
    //        isRefreshCalled = true
    //        getUsersCurrentLocation()
    //
    //    }
    
    override func viewWillAppear(_ animated: Bool) {
        statusTableView.reloadData()
        IsViewWillAppear = true
        chypiCheck = 0
        //        isSideMenuButtonAlreadySelected = true
//        getUsersCurrentLocation()
    }
    
    func refreshMenu() {
        
        if globalDataArray.count != 0 {
            
            for i in 1...globalDataArray.count {
                
                ////////////////////////////////////////////
                let object =  globalDataArray[i - 1] as! catAndStatusData
                
                for i in 0..<dummyStatus.count
                {
                    let tempObject = dummyStatus[i] as? catAndStatusData
                    if tempObject?.category_idCat == object.category_idCat
                    {
                        print(dummyStatus)
                        dummyStatus.remove(at: i)
                        print(dummyStatus)
                        dummyStatus.insert(object, at: i)
                        print(dummyStatus)
                        //                    cell.catView.backgroundColor = colorInUIcolor
                        //                    cell.catLabel.textColor = UIColor.white
                        break
                    }
                }
                
            }
            
        }
        statusTableView.reloadData()
        ////////////////////////////////////////////
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == menuTableView {
            return globalDataArray.count + 1
        } else {
            return dummyStatus.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == menuTableView {
            
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "linecell")
                return cell!
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "labelcell") as! HomeScreenMenuTableViewCell
                
                let object =  globalDataArray[indexPath.row - 1] as! catAndStatusData
                
                cell.catLabel.text = object.titleCat!
                let colorInHex = object.colorCat!
                let colorInUIcolor = self.hexStringToUIColor(hex: colorInHex)
                
                cell.catLabel.textColor = colorInUIcolor
                cell.catBar.backgroundColor = colorInUIcolor
                cell.catView.layer.cornerRadius = (cell.catView.layer.frame.size.height) / 2
                cell.catView.clipsToBounds = true
                cell.catView.layer.borderWidth = 1.0
                cell.catView.layer.borderColor = colorInUIcolor.cgColor
                
                cell.catView.backgroundColor = UIColor.clear
                cell.catLabel.textColor = colorInUIcolor
                for i in 0..<dummyStatus.count
                {
                    let tempObject = dummyStatus[i] as? catAndStatusData
                    if tempObject?.category_idCat == object.category_idCat
                    {
                        print(dummyStatus)
                        dummyStatus.remove(at: i)
                        print(dummyStatus)
                        dummyStatus.insert(object, at: i)
                        print(dummyStatus)
                        cell.catView.backgroundColor = colorInUIcolor
                        cell.catLabel.textColor = UIColor.white
                        break
                    }
                }
                
                statusTableView.reloadData()
                return cell
            }
            
        } else {
            
            if let spacer = tableView.reorder.spacerCell(for: indexPath) {
                return spacer
            }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! HomeScreenStatusTableViewCell
            
            
            cell.collectionView.tag = indexPath.row
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.reloadData()
            
            let object = dummyStatus[indexPath.row] as? catAndStatusData
            
            let colorInHex = object!.colorCat!
            let colorInUIcolor = self.hexStringToUIColor(hex: colorInHex)
            
            cell.categoryName.textColor = colorInUIcolor
            cell.removeCategoryButton.tintColor = colorInUIcolor
            cell.categoryName.text = object?.titleCat
            
            cell.removeCategoryButton.tag = indexPath.row
            cell.removeCategoryButton.addTarget(self, action: #selector(removeCategoryButtonAction(sender:)), for: .touchUpInside)
            
            return cell
            
        }
        
    }
    
    @objc func removeCategoryButtonAction(sender: UIButton)
    {
        //let object = dummyStatus[sender.tag] as! catAndStatusData
        dummyStatus.remove(at: sender.tag)
        statusTableView.reloadData()
        menuTableView.reloadData()
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == menuTableView
        {
            return  UITableView.automaticDimension
        }
        return 180
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if tableView == menuTableView {
            //            if !isSideMenuButtonAlreadySelected {
            //                tableViewCellAnimation(cell: cell, duration: 1.0, xAxis: 0, yAxis: -250, zAxis: 0)
            //            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if !checkInternet() {
            return
        }
        
        if tableView == menuTableView {
            
            
            
            let object = self.globalDataArray[indexPath.row - 1]  as! catAndStatusData
            
            if dummyStatus.count > 0
            {
                var isDeleted =  false
                let tempArray = dummyStatus
                for i in 0..<tempArray.count
                {
                    let tempObject = dummyStatus[i] as? catAndStatusData
                    if tempObject?.category_idCat == object.category_idCat
                    {
                        unSelectedColorOfMenuTableView(indexPath: indexPath as NSIndexPath, object: object)
                        isDeleted = true
                        dummyStatus.remove(at: i)
                        
                        break
                    }
                    
                }
                if !isDeleted
                {
                    
                    if dummyStatus.count < 5 {
                        
                        selectedColorOfMenuTableView(indexPath: indexPath as NSIndexPath, object: object)
                        
                        dummyStatus.append(object)
                    }
                        
                        
                    else {
                        //let cell  = tableView.cellForRow(at: indexPath) as! HomeScreenMenuTableViewCell
                        
                        showAlertForForm(title: "Info!", message: "You can select only 5 categories at a time", isPopToRoot: false)
                    }
                    
                    //                        if object.user_user.count == 0
                    //                        {
                    //                            showAlertForForm(title: "Error", message: "No business is avaiable for this category in your area", isPopToRoot: false)
                    //                        }
                    //                        else
                    //                        {
                    //                            selectedColorOfMenuTableView(indexPath: indexPath as NSIndexPath, object: object)
                    //
                    //                            dummyStatus.append(object)
                    //
                    //                        }
                    
                }
                
            }
            else
            {
                
                selectedColorOfMenuTableView(indexPath: indexPath as NSIndexPath, object: object)
                dummyStatus.append(object)
                
                //                    if object.user_user.count == 0
                //                    {
                //                        showAlertForForm(title: "Error", message: "No business is avaiable for this category in your area", isPopToRoot: false)
                //                    }
                //                    else
                //                    {
                //
                //                    selectedColorOfMenuTableView(indexPath: indexPath as NSIndexPath, object: object)
                //                    dummyStatus.append(object)
                //
                //                    }
            }
            
            statusTableView.reloadData()
            
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let object = dummyStatus[collectionView.tag] as! catAndStatusData
        return object.user_user.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! HomeScreenStatusCollectionViewCell
        
        
        cell.statusView.layer.cornerRadius = cell.statusView.frame.height / 2
        cell.statusView.clipsToBounds = true
        cell.statusView.layer.masksToBounds = true
        
        cell.statusImage.layer.cornerRadius = cell.statusImage.frame.height / 2
        cell.statusImage.clipsToBounds = true
        cell.statusImage.layer.masksToBounds = true
        cell.statusImage.layer.borderWidth = 2
        cell.statusImage.layer.borderColor = UIColor.white.cgColor
        
        let object = dummyStatus[collectionView.tag] as! catAndStatusData
        
        let userObject = object.user_user[indexPath.item] as! catAndStatusData
        
        if !(userObject.user_status.isEmpty) {
            
            let statusObject = userObject.user_status
            
            let firstStatus = statusObject.first! as! catAndStatusData
            
            cell.statusImage.image = nil
            
            let imageUrl = URL(string: IMAGE_URL + firstStatus.status_imageStatus!)
            
            cell.statusImage.kf.indicatorType = .activity
            
            cell.statusImage.kf.setImage(with: imageUrl, completionHandler: {
                (image, error, cacheType, imageUrl) in
                
                cell.statusTitle.text = firstStatus.business_nameStatus
                
            })
            
            if userObject.over_all_viewedUser! == 1 || firstStatus.is_viewedStatus! == 1 {
                cell.statusView.layer.borderColor = #colorLiteral(red: 0.5333333333, green: 0.5333333333, blue: 0.5333333333, alpha: 1)
                cell.statusView.layer.borderWidth = 1
            } else {
                cell.statusView.layer.borderColor = #colorLiteral(red: 0.337254902, green: 0.5843137255, blue: 0.9254901961, alpha: 1)
                cell.statusView.layer.borderWidth = 2
            }
            
        } else {
            cell.statusImage.image = nil
            cell.statusTitle.text = userObject.business_nameUser
        }
        
        //////////////////////////////////////////////////
        //        let userInfo = object.user_user
        //        print(object.user_user.count)
        //        for item in userInfo {
        //            let statussObj = item as! catAndStatusData
        //            print(statussObj.user_status.count)
        //        }
        //////////////////////////////////////////////////
        
        
        
        
        
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if !checkInternet() {
            return
        }
        
        let object = dummyStatus[collectionView.tag] as! catAndStatusData
        
        let userObject = object.user_user[indexPath.item] as! catAndStatusData
        
        if !(userObject.user_status.isEmpty) {
            
            let statusObject = userObject.user_status
            
            let push = self.storyboard?.instantiateViewController(withIdentifier: "gotoShowStatusVCWithCollectionView") as! showStatusVCWithCollectionView
//            push.delegateInShowStatusVC = self
            push.allBusinessStatus = statusObject
            navigationController?.pushViewController(push, animated: true)
            
        } else {
            showAlertForForm(title: "Error", message: "No status is avaiable for this business in your area", isPopToRoot: false)
        }
        
    }
    
    @IBAction func sideMenuButton(_ sender: UIButton) {
        
        if !checkInternet() {
            return
        }
        
        if sideMenuButton.isSelected == true {
            
            IsViewWillAppear = false
            chypiCheck = 0
            getUsersCurrentLocation()
            
        } else {
            
            sideMenuButtonIsNotSelected()
        }
        
    }
    
    func sideMenuButtonIsSelected() {
        
        isDropDown = true
        //        isSideMenuButtonAlreadySelected = false
        
        sideMenuButton.isSelected = false
        menuTableView.isHidden = false
        
        homeControllerTableViewRightConstraint.constant = -200
        sideMenuEqualHeightConstraint.constant = 150
        rightCircleConstraint.constant = 150
        leftCircleTopConstraint.constant = 300
        leftCircleBottomConstraint.constant = -145
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        
        sideMenuButton.setImage(nil, for: .normal)
        sideMenuButton.setBackgroundImage(#imageLiteral(resourceName: "Box"), for: .normal)
        
    }
    
    func sideMenuButtonIsNotSelected() {
        
        isDropDown = false
        
        menuTableView.isHidden = true
        sideMenuButton.isSelected = true
        
        homeControllerTableViewRightConstraint.constant = 0
        sideMenuEqualHeightConstraint.constant = 0
        rightCircleConstraint.constant = 10
        leftCircleTopConstraint.constant = 160
        leftCircleBottomConstraint.constant = -5
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
        
        sideMenuButton.setImage(#imageLiteral(resourceName: "colorBox"), for: .normal)
        sideMenuButton.setBackgroundImage(#imageLiteral(resourceName: "leftTopButton"), for: .normal)
        
    }
    
    @IBAction func profileButton(_ sender: UIButton) {
        
        if !checkInternet() {
            return
        }
        
        if UserDefaults.standard.value(forKey: "userInfoDict") != nil {
            
            let push = self.storyboard?.instantiateViewController(withIdentifier: "gotoProfileViewController") as! profileViewController
            navigationController?.pushViewController(push, animated: true)
            
        } else {
            let push = self.storyboard?.instantiateViewController(withIdentifier: "gotoLoginSignUpViewController") as! loginSignUpViewController
            navigationController?.pushViewController(push, animated: true)
        }
        
    }
    
    func getUsersCurrentLocation() {
        
        startActivityIndicator(activityIndicator: activityIndicator)
        
        self.placesClient.currentPlace(callback: { (placeLikelihoodList, error) -> Void in
            if let error = error {
                
                self.stopActivityIndicator(activityIndicator: self.activityIndicator)
                //                self.refreshControl.endRefreshing()
                self.showAlertForForm(title: "ERROR", message: error.localizedDescription, isPopToRoot: false)
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            if let placeLikelihoodList = placeLikelihoodList {
                for likelihood in placeLikelihoodList.likelihoods {
                    let place = likelihood.place
                    print("Current Place name \(place.name) at likelihood \(likelihood.likelihood)")
                    print("Current Place address \(String(describing: place.formattedAddress))")
                    print("Current Place Coordinates \(place.coordinate)")
                    self.lat = String(describing: place.coordinate.latitude)
                    self.long = String(describing: place.coordinate.longitude)
                    
                    if self.chypiCheck == 0 {
                        self.getAllCategories()
                    }
                    
                }
                //                self.refreshControl.endRefreshing()
            }
            
        })
    }
    
    func getAllCategories() {
        
        if !checkInternet() {
            return
        }
        
        if lat != nil && long != nil {
            
            self.chypiCheck += 1
            
            url = BASE_URL + GET_ALL_CATEGORIES
            
            parameters["location_latitude"] = lat
            parameters["location_longitude"] = long
            parameters["user_id"] = getGuestID()
            
            ApiManager.sharedInstance.alamofireGetRequest(url: url, parameters: parameters, successCallback: { (dict) in
                
                self.stopActivityIndicator(activityIndicator: self.activityIndicator)
                
                self.isTimerTime = true
                print(dict)
                
                self.globalDataArray = catAndStatusData.parseCategoriesInfo(dict: dict)
                
                if self.IsViewWillAppear {
                    self.refreshMenu()
                } else {
                    self.sideMenuButtonIsSelected()
                }
                
                self.menuTableView.reloadData()
                
                if self.isDropDown {
                    self.perform(#selector(self.tableViewAnimation), with: self, afterDelay: 0.1)
                    self.isDropDown = false
                }
                //                self.perform(#selector(self.changeTheValueOfRefreshBoolean), with: self, afterDelay: 5)
                
            }) { (error) in
                self.stopActivityIndicator(activityIndicator: self.activityIndicator)
                self.showAlertForForm(title: "Error", message: error, isPopToRoot: false)
            }
            
        } else {
            self.stopActivityIndicator(activityIndicator: self.activityIndicator)
            showAlertForForm(title: "Error!", message: "Location is unknown.", isPopToRoot: false)
        }
    }
    
    @objc func tableViewAnimation() {
        
        for i in 0...self.globalDataArray.count {
            guard let cell = self.menuTableView.cellForRow(at: IndexPath(item: i, section: 0)) else { return }
            self.tableViewCellAnimation(cell: cell, duration: 0.7, xAxis: 0, yAxis: -250, zAxis: 0)
            
        }
        
    }
    
    //    @objc func changeTheValueOfRefreshBoolean()
    //    {
    //        isRefreshCalled  = false
    //    }
    
    
    func selectedColorOfMenuTableView(indexPath:NSIndexPath , object:catAndStatusData)
    {
        let cell = menuTableView.cellForRow(at: indexPath as IndexPath) as! HomeScreenMenuTableViewCell
        let colorInHex = object.colorCat!
        let colorInUIcolor = self.hexStringToUIColor(hex: colorInHex)
        cell.catView.backgroundColor = colorInUIcolor
        cell.catLabel.textColor = UIColor.white
    }
    
    func unSelectedColorOfMenuTableView(indexPath:NSIndexPath , object:catAndStatusData)
    {
        
        let cell =  menuTableView.cellForRow(at: indexPath as IndexPath) as! HomeScreenMenuTableViewCell
        let colorInHex = object.colorCat!
        let colorInUIcolor = self.hexStringToUIColor(hex: colorInHex)
        cell.catView.backgroundColor = .clear
        cell.catLabel.textColor = colorInUIcolor
    }
    
    func timerRefresher() {
        Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(updateTimerRefresher), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimerRefresher() {
        if isTimerTime {
            self.IsViewWillAppear = true
            //            self.isSideMenuButtonAlreadySelected = true
            self.getAllCategories()
        }
    }
    
}

extension HomeScreenViewControllerNew: TableViewReorderDelegate {
    func tableView(_ tableView: UITableView, reorderRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        
        let movedObject = self.dummyStatus[sourceIndexPath.row]
        dummyStatus.remove(at: sourceIndexPath.row)
        dummyStatus.insert(movedObject, at: destinationIndexPath.row)
        statusTableView.reloadData()
        
    }
}
