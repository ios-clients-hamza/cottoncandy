import UIKit
import GooglePlacePicker
import DropDown

class signUpVC: BaseClass, UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout, CLLocationManagerDelegate, GMSPlacePickerViewControllerDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let locationManager = CLLocationManager()
    let geoCoder = CLGeocoder()
    var formattedAddressFromCoords: String = ""
    
    var indexPath: IndexPath = NSIndexPath(row: 1, section: 0) as IndexPath
    var indexPathForForm: IndexPath = IndexPath(item: 0, section: 0)
    
    let dropDown = DropDown()
    
    var url: String = ""
    
    var bussinessName: String = ""
    var category: String = ""
    var categoryID: String = ""
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var businessAddress: String = ""
    var bussinessPhoneNo: String = ""
    var email: String = ""
    var password: String = ""
    var rePassword: String = ""
    var parameters = [String: Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        roundTopView(view: topView)
        roundTopView(view: whiteView)
        
        hideKeyboard()
        
        self.indexPath.row = 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0 {
            
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath) as! signUpCollectionViewCell
            cell1.cell1NextButtonView.layer.cornerRadius = cell1.cell1NextButtonView.frame.height / 2
            
            cell1.cell1NextButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            
            dropDown.anchorView = cell1.cell1CategoryView
            dropDown.selectionAction = { (index: Int, item: String) in
                cell1.cell1CategoryTextField.text = item
                self.categoryID = String(describing: index + 1)
            }
            
            cell1.cell1CategoryTextField.addTarget(self, action: #selector(handleCatTapped), for: .editingDidBegin)
            
            return cell1
            
        } else if indexPath.row == 1 {
            
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath) as! signUpCollectionViewCell
            cell2.cell2NextButtonView.layer.cornerRadius = cell2.cell2NextButtonView.frame.height / 2
            
            cell2.cell2NextButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            
            cell2.cell2PlacePickerMapButton.addTarget(self, action: #selector(cell2PlacePickerMapButton), for: .touchUpInside)
            
            return cell2
            
        } else {
            
            let cell3 = collectionView.dequeueReusableCell(withReuseIdentifier: "cell3", for: indexPath) as! signUpCollectionViewCell
            cell3.cell3NextButtonView.layer.cornerRadius = cell3.cell3NextButtonView.frame.height / 2
            
            cell3.cell3NextButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            
            return cell3
            
        }
        
    }
    
    @objc func handleCatTapped() {
        
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
        
        dropDown.dataSource = ["Food", "Nightlife+Pubs", "Clothing", "Beauty+Spas", "Health+Fitness", "Travel", "Automotive", "Fun To Do", "Makeup", "Classes+Courses", "Restaurant", "Medical"]
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
        dropDown.show()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = UIScreen.main.bounds.width
        let height = self.collectionView.frame.size.height
        
        if indexPath.row == 2
        {
            return CGSize(width: width, height: height + 4)
        }
        return CGSize(width: width, height: height)
    }
    
    @objc func cell2PlacePickerMapButton() {
        
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        
        present(placePicker, animated: true, completion: nil)
        
    }
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        
        if place.formattedAddress != nil {
            
            latitude = place.coordinate.latitude
            longitude = place.coordinate.longitude
            
            let cell2 = collectionView.cellForItem(at: IndexPath(item: 1, section: 0)) as! signUpCollectionViewCell
            cell2.cell2BusinessAddressTextField.text = place.name + ", " + place.formattedAddress!
            
            viewController.dismiss(animated: true, completion: nil)
            
        } else {
            parseAddress(coords: place.coordinate, viewController: viewController)
        }
        
    }
    
    func parseAddress(coords: CLLocationCoordinate2D, viewController: GMSPlacePickerViewController) {
        
        let location = CLLocation(latitude: coords.latitude, longitude: coords.longitude)
        
        geoCoder.reverseGeocodeLocation(location) { (response, error) in
            print(response!)
            
            if let name = response?.last?.name {
                print(name)
                self.formattedAddressFromCoords = name
            }
            
            if let subLocality = response?.last?.subLocality {
                print(subLocality)
                self.formattedAddressFromCoords.append(", " + subLocality)
            }
            
            if let locality = response?.last?.locality {
                print(locality)
                self.formattedAddressFromCoords.append(", " + locality)
            }
            
            if let administrativeArea = response?.last?.administrativeArea {
                print(administrativeArea)
                self.formattedAddressFromCoords.append(", " + administrativeArea)
            }
            
            if let postalCode = response?.last?.postalCode {
                print(postalCode)
                self.formattedAddressFromCoords.append(", " + postalCode)
            }
            
            if let country = response?.last?.country {
                print(country)
                self.formattedAddressFromCoords.append(", " + country)
            }
            
            if !self.formattedAddressFromCoords.isEmpty {
                
                viewController.dismiss(animated: true, completion: nil)
                let cell2 = self.collectionView.cellForItem(at: IndexPath(item: 1, section: 0)) as! signUpCollectionViewCell
                cell2.cell2BusinessAddressTextField.text = self.formattedAddressFromCoords
                
            } else {
                print("Cannot find location at this point")
            }
            
        }
        
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        
        viewController.dismiss(animated: true, completion: nil)
        print("No place is selected")
        
    }
    
    @objc func buttonAction() {
        submitForm()
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    func submitForm() {
        
        if indexPathForForm.item == 0 {
            
            if !checkInternet() {
                return
            }
            
            let cell = collectionView.cellForItem(at: indexPathForForm) as! signUpCollectionViewCell
            
            if !(cell.cell1BusinessNameTextField.text?.isEmpty)! && !(cell.cell1CategoryTextField.text?.isEmpty)! {
                
                bussinessName = cell.cell1BusinessNameTextField.text!
                category = cell.cell1CategoryTextField.text!
                
                scrollToItem()
                
            } else {
                showAlertForForm(title: "Error", message: "Fill the required fields to continue", isPopToRoot: false)
            }
            
        } else if indexPathForForm.item == 1 {
            
            if !checkInternet() {
                return
            }
            
            let cell = collectionView.cellForItem(at: indexPathForForm) as! signUpCollectionViewCell
            
            if !(cell.cell2BusinessAddressTextField.text?.isEmpty)! && !(cell.cell2BusinessPhoneNoTextField.text?.isEmpty)! {
                
                businessAddress = cell.cell2BusinessAddressTextField.text!
                bussinessPhoneNo = cell.cell2BusinessPhoneNoTextField.text!
                
                scrollToItem()
                
            } else {
                showAlertForForm(title: "Error", message: "Fill the required fields to continue", isPopToRoot: false)
            }
            
        } else if indexPathForForm.item == 2 {
            
            if !checkInternet() {
                return
            }
            
            let cell = collectionView.cellForItem(at: indexPathForForm) as! signUpCollectionViewCell
            
            if !(cell.cell3EmailTextField.text?.isEmpty)! && isValidEmail(email: cell.cell3EmailTextField.text!){
                
                if !(cell.cell3PasswordTextField.text?.isEmpty)! && !(cell.cell3RePasswordTextField.text?.isEmpty)! {
                    
                    if (cell.cell3PasswordTextField.text)! == (cell.cell3RePasswordTextField.text)! {
                        
                        email = cell.cell3EmailTextField.text!
                        password = cell.cell3PasswordTextField.text!
                        rePassword = cell.cell3RePasswordTextField.text!
                        
                        url = BASE_URL + SIGN_UP
                        
                        parameters["business_name"] = bussinessName
                        parameters["category_id"] = categoryID
                        parameters["address"] = businessAddress
                        parameters["user_latitude"] = latitude
                        parameters["user_longitude"] = longitude
                        parameters["phone"] = bussinessPhoneNo
                        parameters["email"] = email
                        parameters["password"] = password
                        
                        startActivityIndicator(activityIndicator: activityIndicator)
                        
                        ApiManager.sharedInstance.alamofirePostRequest(urlString: url, parameters: parameters, successCallback: { (dict) in
                            
                            print(dict)
                        
                            let dictData = dict["user_info"] as! NSDictionary
                            
                            self.stopActivityIndicator(activityIndicator: self.activityIndicator)
                            
                            self.setUserDataInUserDefaults(dict: dictData)
                            
                            self.showAlertForForm(title: "Congratulations!", message: "Your Account Has Been Created Succesfully", isPopToRoot: true)
                            
                        }) { (error) in
                            
                            self.stopActivityIndicator(activityIndicator: self.activityIndicator)
                            self.showAlertForForm(title: "Error", message: error, isPopToRoot: false)
                        }
                        
                    } else {
                        showAlertForForm(title: "Error", message: "Password does not match", isPopToRoot: false)
                    }
                    
                } else {
                    showAlertForForm(title: "Error", message: "Fill the required fields to continue", isPopToRoot: false)
                }
                
            } else {
                showAlertForForm(title: "Error", message: "Email is not valid", isPopToRoot: false)
            }
            
        }
        
    }
    
    func scrollToItem() {
        
        self.indexPathForForm.item += 1
        self.collectionView.scrollToItem(at: self.indexPath, at: .left, animated: true)
        self.indexPath.row += 1
    }
    
}
