import UIKit
//import IQKeyboardManager

class forgetPasswordViewController: BaseClass {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var url: String = ""
    var parameters = [String: Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        IQKeyboardManager.shared().isEnabled = false
        roundTopView(view: topView)
        roundTopView(view: whiteView)
        
        hideKeyboard()
        
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitButton(_ sender: UIButton) {
        
        if !checkInternet() {
            return
        }
        
        if !(email.text?.isEmpty)! && isValidEmail(email: email.text!) {
            
            url = BASE_URL + FORGOT_PASSWORD
            let email: String = self.email.text!
            
            parameters["email"] = email
            
            startActivityIndicator(activityIndicator: activityIndicator)
            
            ApiManager.sharedInstance.alamofirePostRequest(urlString: url, parameters: parameters, successCallback: { (dict) in
                print(dict)
                
                let dictData = dict["message"] as! String
                
                self.stopActivityIndicator(activityIndicator: self.activityIndicator)
                
                self.showAlertForForm(title: "Info", message: dictData, isPopToRoot: true)
                
            }) { (error) in
                
                self.stopActivityIndicator(activityIndicator: self.activityIndicator)
                self.showAlertForForm(title: "Error", message: error, isPopToRoot: false)
                
            }
        } else {
            showAlertForForm(title: "Error", message: "Email is not valid", isPopToRoot: false)
        }
        
    }
    
}
