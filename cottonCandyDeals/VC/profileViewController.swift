import UIKit
import Photos

class profileViewController: BaseClass, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var addImageView: ShadowView!
    @IBOutlet weak var addImageButton: UIButton!
    @IBOutlet weak var itemNameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var selectedImage: UIImage? = nil
    
    var url: String = ""
    var parameters = [String: Any]()
    var allStatus = Array<Any>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        roundTopView(view: topView)
        roundTopView(view: whiteView)
        
        image.layer.cornerRadius = image.frame.height / 2
        image.clipsToBounds = true
        image.layer.masksToBounds = true
        image.layer.borderWidth = 1
        image.layer.borderColor = UIColor.darkGray.cgColor
        
        addImageButton.layer.cornerRadius = addImageButton.frame.height / 2
        addImageButton.clipsToBounds = true
        addImageButton.layer.masksToBounds = true
        
        addImageView.layer.cornerRadius = addImageView.frame.height / 2
        
        showUserStatus()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let userInfoDict = getUserDataFromUserDefaults()
        
        itemNameTextField.text = userInfoDict["business_name"] as? String
        addressTextField.text = userInfoDict["address"] as? String
        phoneNumberTextField.text = userInfoDict["phone"] as? String
        emailTextField.text = userInfoDict["email"] as? String
        categoryTextField.text = userInfoDict["category_title"] as? String
        let colorInHex = userInfoDict["color"] as! String
        
        let colorInUIcolor = self.hexStringToUIColor(hex: colorInHex)
        categoryTextField.textColor = colorInUIcolor
        
        getProfileStatus(isImageTapped: false)
        
    }
    
    @IBAction func addImageButtonAction(_ sender: UIButton) {
        
        if !checkInternet() {
            return
        }
        
        if allStatus.count < 5 {
            
            let photos = PHPhotoLibrary.authorizationStatus()
            if photos == .notDetermined || photos == .authorized {
                PHPhotoLibrary.requestAuthorization({ status in
                    if status == .authorized {
                        print("access granted")
                        
                        let imagePicker = UIImagePickerController()
                        imagePicker.delegate = self
                        imagePicker.allowsEditing = true
                        self.present(imagePicker, animated: true, completion: nil)
                        
                    } else {
                        print("access denied")
                    }
                })
            } else {
                showAlertForForm(title: "Error", message: "App is not able to access Photos, give permission to continue", isPopToRoot: false)
            }
            
            
        } else {
            showAlertForForm(title: "Error", message: "You can only post 5 status at a time.", isPopToRoot: false)
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        selectedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage
        self.dismiss(animated: true, completion: nil)
        
        let push = self.storyboard?.instantiateViewController(withIdentifier: "gotoAddStatusViewController") as! addStatusViewController
        push.selectedImage = selectedImage
        navigationController?.pushViewController(push, animated: true)
        
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func logoutButton(_ sender: UIButton) {
        
        if !checkInternet() {
            return
        }
        
        showAlertForLogOut()
        
    }
    
    func showAlertForLogOut() {
        
        let actionScheet = UIAlertController(title: "Do you want to Log Out?", message: nil, preferredStyle: .actionSheet)
        
        let logoutButton = UIAlertAction(title: "Logout", style: .destructive) { (action) in
            
            self.removeUserDataFromUserDefaults()
            if UserDefaults.standard.value(forKey: "userProfileStatus") != nil {
                UserDefaults.standard.removeObject(forKey: "userProfileStatus")
            }
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            print("Cancel is pressed")
        }
        
        actionScheet.addAction(logoutButton)
        actionScheet.addAction(cancelButton)
        
        self.present(actionScheet, animated: true, completion: nil)
    }
    
    func showUserStatus() {
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapForShowUserStatus))
        image.addGestureRecognizer(tap)
    }
    
    @objc func handleTapForShowUserStatus() {
        
        getProfileStatus(isImageTapped: true)
        
    }
    
    func getProfileStatus(isImageTapped: Bool) {
        
        if !checkInternet() {
            return
        }
        
        startActivityIndicator(activityIndicator: activityIndicator)
        
        url = BASE_URL + GET_USER_STATUS
        
        let userInfo = UserDefaults.standard.value(forKey: "userInfoDict") as! NSDictionary
        
        parameters["user_id"] = userInfo["user_id"] as! String
        
        ApiManager.sharedInstance.alamofireGetRequest(url: url, parameters: parameters, successCallback: { (dict) in
            
            print(dict)
            
            self.stopActivityIndicator(activityIndicator: self.activityIndicator)
            
            self.allStatus.removeAll()
            
            let object = profileStatusInfo.parseProfileStatusInfo(dict: dict)
            self.allStatus = object.user_status
            
            print(self.allStatus.count)
            
            if isImageTapped {
                
                if !(self.allStatus.isEmpty) {
                    let push = self.storyboard?.instantiateViewController(withIdentifier: "gotoShowStatusVCWithCollectionView") as! showStatusVCWithCollectionView
                    push.isUserStatus = true
                    push.allStatus = self.allStatus
                    self.navigationController?.pushViewController(push, animated: true)
                } else {
                    self.showAlertForForm(title: "Error", message: "You have not posted any status yet", isPopToRoot: false)
                }
                
            } else {
                
                if !(self.allStatus.isEmpty) {
                    
                    let currentStatus = self.allStatus.first as! profileStatusInfo
                    
                    let imageUrl = URL(string: IMAGE_URL + currentStatus.status_imageStatus!)
                    
                    self.image.kf.indicatorType = .activity
                    self.image.kf.setImage(with: imageUrl, completionHandler: {
                        (image, error, cacheType, imageUrl) in
                        
                        if UserDefaults.standard.value(forKey: "userProfileStatus") != nil {
                            UserDefaults.standard.removeObject(forKey: "userProfileStatus")
                        }
                        
                        let imageData: Data = NSKeyedArchiver.archivedData(withRootObject: image!)
                        UserDefaults.standard.set(imageData, forKey: "userProfileStatus")
                        
                        self.image.image = image
                        
                    })
                    
                } else {
                    
                    self.image.image = nil
                    
                    self.stopActivityIndicator(activityIndicator: self.activityIndicator)
                }
                
            }
            
        }) { (error) in
            
            self.stopActivityIndicator(activityIndicator: self.activityIndicator)
            self.showAlertForForm(title: "Error", message: error, isPopToRoot: true)
            
        }
        
    }
    
}
