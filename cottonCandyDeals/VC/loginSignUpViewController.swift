import UIKit

class loginSignUpViewController: BaseClass {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var whiteView: UIView!
    @IBOutlet weak var EmailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var email: String = ""
    var password: String = ""
    var url: String = ""
    var parameters = [String: Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        roundTopView(view: topView)
        roundTopView(view: whiteView)
        
        hideKeyboard()
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func loginButton(_ sender: UIButton) {
        
        if !checkInternet() {
            return
        }
        
        if !(EmailTextField.text?.isEmpty)! && !(passwordTextField.text?.isEmpty)! {
            
            if isValidEmail(email: EmailTextField.text!) {
                
                url = BASE_URL + SIGN_IN
                
                email = EmailTextField.text!
                password = passwordTextField.text!
                
                parameters["email"] = email
                parameters["password"] = password
                
                startActivityIndicator(activityIndicator: activityIndicator)
                
                ApiManager.sharedInstance.alamofirePostRequest(urlString: url, parameters: parameters, successCallback: { (dict) in
                    print(dict)
                    
                    let dictData = dict["user_info"] as! NSDictionary
                    
                    self.stopActivityIndicator(activityIndicator: self.activityIndicator)
                    
                    self.setUserDataInUserDefaults(dict: dictData)
                    
                    self.navigationController?.popToRootViewController(animated: true)
                    
                }) { (error) in
                    
                    self.stopActivityIndicator(activityIndicator: self.activityIndicator)
                    self.showAlertForForm(title: "Error", message: error, isPopToRoot: false)
                    
                }
            } else {
                showAlertForForm(title: "Error", message: "Email is not valid", isPopToRoot: false)
            }
            
        } else {
            showAlertForForm(title: "Error", message: "Fill the required fields to continue", isPopToRoot: false)
        }
        
    }
    
    @IBAction func signUpButton(_ sender: UIButton) {
        
        if !checkInternet() {
            return
        }
        
        let push = self.storyboard?.instantiateViewController(withIdentifier: "gotoSignUpVC") as! signUpVC
        navigationController?.pushViewController(push, animated: true)
    }
    
    @IBAction func forgetPasswordButton(_ sender: UIButton) {
        
        if !checkInternet() {
            return
        }
        
        let push = self.storyboard?.instantiateViewController(withIdentifier: "gotoForgetPasswordViewController") as! forgetPasswordViewController
        navigationController?.pushViewController(push, animated: true)
    }
    
}

