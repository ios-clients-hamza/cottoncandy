import UIKit
import Kingfisher

//protocol dataPassingFromShowStatusVC {
//    func dataPassingFromShowStatus(isStatusViewed: Bool)
//}

class showStatusVCWithCollectionView: BaseClass, UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if isUserStatus {
            return allStatus.count
        } else {
            return allBusinessStatus.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        return cell
    }
    
    @IBOutlet weak var outerImageView: UIImageView!
    @IBOutlet weak var innerImageView: UIImageView!
    @IBOutlet weak var reportDeleteStatus: UIButton!
    @IBOutlet weak var views: UILabel!
    @IBOutlet weak var statusAddress: UILabel!
    @IBOutlet weak var statusPhoneNumber: UILabel!
    @IBOutlet weak var statusDeal: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var chypiView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var timer = Timer()
    var isTimerStopped = false
    var currentTime: Float = 0.0
    let maxTime: Float = 7.0
    var isUserStatus: Bool = false
    
    var url: String = ""
    var parameters = [String: Any]()
    var allStatus = Array<Any>()
    
    var statusCounter: Int = 0
    
    var allBusinessStatus = Array<Any>()
    var statusID: String = ""
    
    var indexPath = IndexPath(item: 0, section: 0)
    var indexPathForNextStatus = IndexPath(item: 0, section: 0)
    
//    var delegateInShowStatusVC: dataPassingFromShowStatusVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        outerImageView.clipsToBounds = true
        
        nextStatus()
        //endStatus()
        pauseStatusOnLongPress()
        
        if isUserStatus {
            reportDeleteStatus.setImage(#imageLiteral(resourceName: "trash"), for: .normal)
            adjustStatusBar(CollectionViewOutlet: collectionView, totalBars: allStatus.count)
        } else {
            reportDeleteStatus.setImage(#imageLiteral(resourceName: "flag"), for: .normal)
            adjustStatusBar(CollectionViewOutlet: collectionView, totalBars: allBusinessStatus.count)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showStatus()
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        delegateInShowStatusVC?.dataPassingFromShowStatus(isStatusViewed: true)
//    }
    
    @objc func updateTimer() {
        if isTimerStopped {
            return
        } else {
            currentTime += 0.01
            if currentTime <= 7.0 {
                let cell = collectionView.cellForItem(at: indexPath) as! statusBarCollectionViewCell
                cell.progressView.setProgress(currentTime / maxTime, animated: true)
                
            } else {

                if isUserStatus {
                    if statusCounter < allStatus.count - 1 {
                        let cell = collectionView.cellForItem(at: indexPath) as! statusBarCollectionViewCell
                        cell.progressView.setProgress(7.0, animated: false)
                        currentTime = 0.0
                        indexPath.row += 1
                        statusCounter += 1
                        showStatus()
                    } else {
                        navigationController?.popViewController(animated: true)
                    }
                } else {
                    if statusCounter < allBusinessStatus.count - 1 {
                        
                        let currentStatus = allBusinessStatus[statusCounter] as! catAndStatusData
                        currentStatus.is_viewedStatus = 1
                        
                        let cell = collectionView.cellForItem(at: indexPath) as! statusBarCollectionViewCell
                        cell.progressView.setProgress(7.0, animated: false)
                        currentTime = 0.0
                        indexPath.row += 1
                        statusCounter += 1
                        showStatus()
                        
                    } else {
                        navigationController?.popViewController(animated: true)
                    }
                }


            }
        }
    }
    
    @IBAction func deleteReportStatus(_ sender: UIButton) {
        
        if !checkInternet() {
            return
        }
        
        if isUserStatus {
            deleteStatus()
        } else {
            reportStatus()
        }
        
    }
    
    func deleteStatus() {
        
        stopTimer()
        
        let alert = UIAlertController(title: nil, message: "Do you want to delete this status?", preferredStyle: .alert)
        
        let cancelButton = UIAlertAction(title: "No", style: .cancel) { (action) in
            print("Cancel button is pressed")
            self.startTimer()
        }
        
        let deleteButton = UIAlertAction(title: "Yes", style: .destructive) { (action) in
            self.deleteStatusProcess()
        }
        
        alert.addAction(cancelButton)
        alert.addAction(deleteButton)
        present(alert, animated: true, completion: nil)
        
    }
    
    func deleteStatusProcess() {
        
        startActivityIndicator(activityIndicator: activityIndicator)
        
        var parameters = [String: Any]()
        
        let currentStatus = allStatus[statusCounter] as! profileStatusInfo
        
        parameters["user_id"] = currentStatus.user_idStatus
        parameters["user_status_id"] = currentStatus.user_status_idStatus
        
        print(parameters)
        
        let url = BASE_URL + DELETE_STATUS
        
        ApiManager.sharedInstance.alamofirePostRequest(urlString: url, parameters: parameters, successCallback: { (dict) in
            
            print(dict)
            
            self.stopActivityIndicator(activityIndicator: self.activityIndicator)
            self.navigationController?.popViewController(animated: true)
            
        }) { (error) in
            self.stopActivityIndicator(activityIndicator: self.activityIndicator)
            self.showAlertForForm(title: "Error", message: error, isPopToRoot: true)
        }
        
    }
    
    func reportStatus() {
        
        stopTimer()
        
        let alert = UIAlertController(title: nil, message: "Do you want to report this item?", preferredStyle: .alert)
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            print("Cancel button is pressed")
            self.startTimer()
        }
        
        let reportButton = UIAlertAction(title: "Submit", style: .destructive) { (action) in
            self.terminateStatus()
        }
        
        alert.addAction(cancelButton)
        alert.addAction(reportButton)
        present(alert, animated: true, completion: nil)
        
    }
    
    func terminateStatus() {
        
        startActivityIndicator(activityIndicator: activityIndicator)
        
        var parameters = [String: Any]()
        
        if UserDefaults.standard.value(forKey: "userInfoDict") != nil {
            let userInfo = UserDefaults.standard.value(forKey: "userInfoDict") as! NSDictionary
            
            parameters["reported_by"] = userInfo["user_id"] as! String
        } else {
            parameters["reported_by"] = getGuestID()
        }
        
        parameters["report_type"] = "status"
        parameters["user_or_status_id"] = statusID
        
        
        print(parameters)
        
        let url = BASE_URL + REPORT_STATUS
        
        ApiManager.sharedInstance.alamofirePostRequest(urlString: url, parameters: parameters, successCallback: { (dict) in
            
            print(dict)
            
            self.stopActivityIndicator(activityIndicator: self.activityIndicator)
            self.showAlertForForm(title: "Success", message: dict["message"] as! String, isPopToRoot: true)
            
        }) { (error) in
            self.stopActivityIndicator(activityIndicator: self.activityIndicator)
            self.showAlertForForm(title: "Error", message: error, isPopToRoot: true)
        }
        
    }
    
    func stopTimer() {
        isTimerStopped =  true
    }
    
    func startTimer() {
        isTimerStopped = false
    }
    
    func pauseStatusOnLongPress() {
        
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPressForStatus))
        lpgr.minimumPressDuration = 0.1
        self.view.addGestureRecognizer(lpgr)
        
    }
    
    @objc func handleLongPressForStatus(gestureReconizer: UILongPressGestureRecognizer) {
        if gestureReconizer.state != UIGestureRecognizer.State.ended {
            stopTimer()
        } else {
            startTimer()
        }
    }
    
    func showStatus() {

        if isUserStatus {

            self.startActivityIndicator(activityIndicator: activityIndicator)
            self.chypiView.isHidden = false
            stopTimer()

            print(allStatus.count)
            let currentStatus = allStatus[statusCounter] as! profileStatusInfo

            if currentStatus.total_viewedStatus != nil {
                self.views.text = String(describing: currentStatus.total_viewedStatus!)
            } else {
                self.views.text = "0"
            }

            let imageUrl = URL(string: IMAGE_URL + currentStatus.status_imageStatus!)
            self.innerImageView.kf.indicatorType = .activity

            self.outerImageView.kf.setImage(with: imageUrl, completionHandler: {
                (image, error, cacheType, imageUrl) in

                self.innerImageView.kf.setImage(with: imageUrl, completionHandler: {
                    (image, error, cacheType, imageUrl) in

                    self.statusAddress.text = currentStatus.addressStatus!
                    self.statusPhoneNumber.text = currentStatus.phoneStatus!
                    self.statusDeal.text = currentStatus.deal_forStatus!

                    self.chypiView.isHidden = true
                    self.stopActivityIndicator(activityIndicator: self.activityIndicator)
                    self.timer.invalidate()
                    self.timerCounter()
                    self.startTimer()

                })

            })

        } else {

            self.startActivityIndicator(activityIndicator: activityIndicator)
            self.chypiView.isHidden = false
            stopTimer()

            print(allBusinessStatus.count)
            let currentStatus = allBusinessStatus[statusCounter] as! catAndStatusData

            statusID = currentStatus.user_status_idStatus!

            if currentStatus.total_viewedStatus != nil {
                self.views.text = String(describing: currentStatus.total_viewedStatus!)
            } else {
                self.views.text = "0"
            }

            let imageUrl = URL(string: IMAGE_URL + currentStatus.status_imageStatus!)
            self.innerImageView.kf.indicatorType = .activity

            self.outerImageView.kf.setImage(with: imageUrl, completionHandler: {
                (image, error, cacheType, imageUrl) in

                self.innerImageView.kf.setImage(with: imageUrl, completionHandler: {
                    (image, error, cacheType, imageUrl) in

                    if currentStatus.is_viewedStatus! == 0 {
                        self.viewStatus()
                    }

                    self.statusAddress.text = currentStatus.addressStatus!
                    self.statusPhoneNumber.text = currentStatus.phoneStatus!
                    self.statusDeal.text = currentStatus.deal_forStatus!

                    self.chypiView.isHidden = true
                    self.stopActivityIndicator(activityIndicator: self.activityIndicator)
                    self.timer.invalidate()
                    self.timerCounter()
                    self.startTimer()

                })

            })

        }

    }
    
    func viewStatus() {
        
        if !checkInternet() {
            return
        }
        
        self.startActivityIndicator(activityIndicator: activityIndicator)
        
        var parameters = [String: Any]()
        
        parameters["user_status_id"] = statusID
        parameters["user_id"] = getGuestID()
        
        print(parameters)
        
        let url = BASE_URL + VIEW_STATUS
        
        ApiManager.sharedInstance.alamofirePostRequest(urlString: url, parameters: parameters, successCallback: { (dict) in
            
            print(dict)
            
            self.stopActivityIndicator(activityIndicator: self.activityIndicator)
            //self.showAlertForForm(title: "Success", message: dict["message"] as! String, isPopToRoot: true)
            
        }) { (error) in
            self.stopActivityIndicator(activityIndicator: self.activityIndicator)
            //self.showAlertForForm(title: "Error", message: error, isPopToRoot: true)
        }
        
    }
    
    func timerCounter() {
        self.timer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
    }
    
    func nextStatus() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapForNextStatus))
        innerImageView.addGestureRecognizer(tap)
    }
    
    @objc func handleTapForNextStatus() {
        currentTime = 7.0
        self.collectionView.scrollToItem(at: self.indexPathForNextStatus, at: .left, animated: true)
        self.indexPathForNextStatus.row += 1
    }
    
}

