import Alamofire

class ApiManager: NSObject {
    
    static let sharedInstance = ApiManager()
    
    func alamofireGetRequest(url: String, parameters: Parameters, successCallback: @escaping (NSDictionary) -> Void, errorCallBack: @escaping (String) -> Void) {
        
        Alamofire.request(url, method: .get, parameters: parameters).responseJSON { response in
            print(response)
            if let json = response.result.value as? [String: Any] {
                print(json)
                if let isSTATUS = json["status"] {
                    let STATUS = isSTATUS as? Bool
                    if STATUS == true {
                        print("OK!")
                        
                        successCallback(json as NSDictionary)
                        
                    } else {
                        let msg = json["message"] as! String
                        errorCallBack(msg)
                    }
                } else {
                    errorCallBack("Status Doesn't Exist")
                }
            }
        }
        
    }
    
    func alamofirePostRequest(urlString: String, parameters : Parameters, successCallback: @escaping (NSDictionary) -> Void, errorCallBack: @escaping (String) -> Void) -> Void {
        
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON {response in
            print(response)
            if let json = response.result.value as? [String: Any] {
                print(json)
                if let isSTATUS = json["status"] {
                    let STATUS = isSTATUS as? Bool
                    if STATUS == true {
                        print("OK!")
                        
                        successCallback(json as NSDictionary)
                        
                    } else {
                        let msg = json["message"] as! String
                        errorCallBack(msg)
                    }
                } else {
                    errorCallBack("Status Doesn't Exist")
                }
            }
        }
        
    }
    
    func alamofirePostRequestWithImage(urlString : String, parameters : Parameters, successCallback : @escaping (NSDictionary) -> Void, errorCallBack : @escaping (String) -> Void) {
        
        var parameters = parameters
        
        let realImage = parameters["status_image"] as! UIImage
        parameters.removeValue(forKey: "status_image")
        let imageData = realImage.jpegData(compressionQuality: 0.6)
        
        let url = try! URLRequest(url: urlString, method: .post)
        
        Alamofire.upload(multipartFormData: { (MultipartFormData) in
            
            for (key, value) in parameters {
                MultipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
            MultipartFormData.append(imageData!, withName: "status_image", fileName: "statusImage.jpg", mimeType: "image/jpeg")
            
            
            
        }, with: url, encodingCompletion: { (result) in
            
            switch result {
 
            case .success(let upload, _, _):
                upload.responseJSON {
                    response in
                    if let json = response.result.value as? [String: Any] {
                        print(json)
                        if let isSTATUS = json["status"] {
                            let STATUS = isSTATUS as? Bool
                            if STATUS == true {
                                print("OK!")
                                
                                successCallback(json as NSDictionary)
                                
                            } else {
                                let msg = json["message"] as! String
                                errorCallBack(msg)
                            }
                        } else {
                            errorCallBack("Status Doesn't Exist")
                        }
                    }
                }
                
            case .failure(let error):
                    print(error)
                
            }
            
        })
        
    }
    
}
