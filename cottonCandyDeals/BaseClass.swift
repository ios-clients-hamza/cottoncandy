import UIKit

class BaseClass: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func tableViewCellAnimation(cell: UITableViewCell, duration: TimeInterval, xAxis: CGFloat, yAxis: CGFloat, zAxis: CGFloat) {
        
        cell.alpha = 0
        let transform = CATransform3DTranslate(CATransform3DIdentity, xAxis, yAxis, zAxis)
        cell.layer.transform = transform
        UIView.animate(withDuration: duration){
            cell.alpha = 1.0
            cell.layer.transform = CATransform3DIdentity
            
        }
    }
    
    func roundTopView(view: UIView) {
        view.layer.cornerRadius = 13
    }
    
    func hideKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        view.endEditing(true)
    }
    
    func endStatus() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapForEndStatus))
        view.addGestureRecognizer(tap)
    }
    
    @objc func handleTapForEndStatus() {
        navigationController?.popViewController(animated: true)
    }
    
    func startActivityIndicator(activityIndicator: UIActivityIndicatorView) {
        activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopActivityIndicator(activityIndicator: UIActivityIndicatorView) {
        activityIndicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    func showAlertForForm(title: String, message: String, isPopToRoot: Bool) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelButton = UIAlertAction(title: "OK!", style: .cancel) { (action) in
            print("Cancel button is pressed")
            
            if isPopToRoot {
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
        
        alert.addAction(cancelButton)
        present(alert, animated: true, completion: nil)
        
    }
    
//    func setUserDataInModel(dict: NSDictionary) {
//
//        let userInfo = UserInformation()
//
//        userInfo.address = dict["address"] as? String
//        userInfo.business_name = dict["business_name"] as? String
//        userInfo.category_id = dict["category_id"] as? String
//        userInfo.category_title = dict["category_title"] as? String
//        userInfo.email = dict["email"] as? String
//        userInfo.phone = dict["phone"] as? String
//        userInfo.user_id = dict["user_id"] as? String
//
//    }
    
    func setUserDataInUserDefaults(dict: NSDictionary) {
        UserDefaults.standard.set(dict, forKey: "userInfoDict")
    }
    
    func getUserDataFromUserDefaults() -> NSDictionary {
        return UserDefaults.standard.object(forKey: "userInfoDict") as! NSDictionary
    }
    
    func removeUserDataFromUserDefaults() {
        UserDefaults.standard.removeObject(forKey: "userInfoDict")
    }
    
    func hexStringToUIColor(hex: String) -> UIColor {
        
        var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func adjustStatusBar(CollectionViewOutlet: UICollectionView, totalBars: Int) {
        
        let width = (UIScreen.main.bounds.width) / CGFloat(totalBars)
        let layout = CollectionViewOutlet.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: width, height: 2)
        
    }
    
    func generateRandomStringForGuestUserOneTime() {
        
        if (UserDefaults.standard.value(forKey: "guestUserID") == nil) {
            let someDate = Int(Date().timeIntervalSince1970 * 1000)
            UserDefaults.standard.set(someDate, forKey: "guestUserID")
            
            print(UserDefaults.standard.value(forKey: "guestUserID")!)
            print("ID has been Stored")
        } else {
            print(UserDefaults.standard.value(forKey: "guestUserID")!)
            print("ID Already Stored")
        }
    
    }
    
    func getGuestID() -> String {
        let id: Int = UserDefaults.standard.value(forKey: "guestUserID") as! Int
        let idInString: String = String(describing: id)
        return idInString
    }
    
    func checkInternet() -> Bool {
        
        if !Reachability.isConnectedToNetwork() {
            showAlertForForm(title: "Internet Connection not  Available!", message: "Please check your internet connection and try again.", isPopToRoot: false)
            return false
        } else {
            return true
        }
        
    }
    
}
